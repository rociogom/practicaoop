class Empleado:

    def __init__(self, nombre, nomina):
        self.nombre = nombre
        self.nomina = nomina
    def calculo_impuestos (self):
        return self.nomina*0.30
    def __str__(self):
        return f"El empleado {self.nombre} debe pagar {self.calculo_impuestos():.2f}"
empleado = Empleado("Pepe", 2000)
empleada = Empleado("Ana",3000)
total = empleado.calculo_impuestos() + empleada.calculo_impuestos()
print(empleado)
print(empleada)
print(f"Los impuestos a pagar en total son {total:.2f} euros")
